let input;

function setup() {
  noCanvas();
  let button = select('#submit');
  button.mousePressed(marvelAsk);
  $("#search").keyup(function(event) {
      if (event.keyCode === 13) {
          marvelAsk();
      }
  });
  input = select('#search');

  let button2 = select('#randos');
  button2.mousePressed(randome);
  
}

function marvelAsk() {
  let nome = input.value().toLowerCase(); 
  if (nome == "marveldex" || nome == "marvel dex"){
      swal({
      title: "MarvelDex",
        text: "Marveldex is a search tool that allows you to know the history and description about any Marvel hero.",
        button: "nobutton",
        imageUrl: "https://stickershop.line-scdn.net/stickershop/v1/product/1063/LINEStorePC/main@2x.png;compress=true",
        backdrop: `
        rgba(0,0,123,0.4)
      `
    })
  }
  else{
  let api = 'https://gateway.marvel.com:443/v1/public/characters?ts=1&name=';
  let apiKey = '&apikey=f5e9599bbcca4c211e6eec1989056fc2&hash=1020e7998e07cdf0e9075bacb6874f5e';
  let url = api + input.value() + apiKey;
  loadJSON(url, gotData);}
}

function randome() {
  swal ({ 
          title: "Loading ...", 
          onOpen: () => {
          swal.showLoading()},
          backdrop: `
            rgba(0,0,123,0.4)
          `
    })
  let urlb = 'https://gateway.marvel.com/v1/public/events?ts=1&apikey=f5e9599bbcca4c211e6eec1989056fc2&hash=1020e7998e07cdf0e9075bacb6874f5e';
  loadJSON(urlb, sorteia);
}

function gotData(data) {
  let marvel = data;
  if (marvel.data.total < 1){
    swal({
      title: "Invalid Search",
      html:"<br> Don't be stupid. <br> <b> Eg:</b> Search 'Spider-man' instead of 'Spyder man'" , 
      width: 600,
      padding: '3em',
      backdrop: `
        rgba(0,0,123,0.4)
        url("https://media.giphy.com/media/3og0IKMyyF26qr6MDe/giphy.gif")
        center top
        no-repeat
      `
    })
  }
  else{
    let image_url = marvel.data.results[0].thumbnail.path;
    let extension = marvel.data.results[0].thumbnail.extension;
    let image_full_url = image_url + '/standard_fantastic.' + extension;
    swal({
      title: marvel.data.results[0].name,
      text:  marvel.data.results[0].description,
      imageUrl: image_full_url,
      backdrop: `
        rgba(0,0,123,0.4)
      `
    });
  }

}

function sorteia(data) {
  let apia = 'https://gateway.marvel.com:443/v1/public/characters?ts=1&name=';
  let apiKeya = '&apikey=f5e9599bbcca4c211e6eec1989056fc2&hash=1020e7998e07cdf0e9075bacb6874f5e';
  let i = Math.floor(Math.random() * 20);
  let j;

  while (data.data.results[i].characters.available == 0) {
    i = Math.floor(Math.random() * 20);
  }
  if (data.data.results[i].characters.available < 20) {
    j = Math.floor(Math.random() * data.data.results[i].characters.available);
  }
  else {
    j = Math.floor(Math.random() * 20);
  }

  let urlc = apia + data.data.results[i].characters.items[j].name + apiKeya;
  loadJSON(urlc, seiLA);
}

function seiLA(data) {
    let image_urla = data.data.results[0].thumbnail.path;
    let extensiona = data.data.results[0].thumbnail.extension;
  
    swal ({
      title: data.data.results[0].name,
      text: data.data.results[0].description,
      imageUrl: image_urla + '/standard_fantastic.' + extensiona,
      backdrop: `
        rgba(0,0,123,0.4)
      `
    })
}